#!/bin/sh

# Architecture (i386, ppc, amd64, etc.)
# if you don't want -src packages that is handled
# in the actual debmirror command line arguments
# with --no-source
arch=i386,amd64

# Section (main,contrib,non-free)
section=main,non-free

# Release of the system (wheezy,jessie,sketch, etc)
# use the real release names here, and create the stable,testing, etc. labels as symbolic links.
# this way when a release happens you don't need to re-download a ton of stuff you already have, just change the links
release=stretch

# Source server name, minus the protocol and the path at the end
# should be one of the HA mirrors
server=www.deb-multimedia.org

# Web path for the above mirror source server, top level of /debian where /debian/dists and /debian/pool can be found
inPath=/

# Protocol to use for transfer (http, ftp, hftp, rsync)
proto=http

# Directory to store the mirror in
# this should show up as the /debian directory
# when your clients access the mirror
outPath=/BuildArea51/debian-mirror/debian-multimedia

# Start script

debmirror -a $arch \
--no-source \
--slow-cpu \
--md5sums \
--progress \
--passive \
--verbose \
-s $section \
-h $server \
-d $release \
-r $inPath \
-e $proto \
$outPath
