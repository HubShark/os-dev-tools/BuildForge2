debmirror
========

The _debmirror_ script is heavily based on the gist found @ [GitHub](https://gist.githubusercontent.com/kleinig/3110783/raw/c2c04465c030e66f605c40321e7de05b6eaa2c26/debmirror.sh)

To use the script just run 

    ./debmirror 

or if your are in the package root (BuildForge)

    ./tools/debmirror


This will mirror Debian Archives ready for **_BuildForge_** usage.


#### Automating the Mirror

Create a cronjob script simular to this:

    nano /etc/cron.d/debmirror

then add this:

    0 2 * * *       root     /root/BuildForge/tools/debmirror > echo "Nightly Backup Successful: $(date)" >> /deb-mirror/nightly-mirror-update.log


make it executable

    chmod 755 /etc/cron.d/debmirror



check-distro-repo-size.sh
=========================

Creates a log with the size of your new distribution. Run this **AFTER** your distribution repository is built.

    ./check-distro-repo-size.sh

or if your are in the package root (BuildForge)

    ./tools/check-distro-repo-size.sh

